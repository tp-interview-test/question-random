import {configureStore} from '@reduxjs/toolkit';

import testerSlice from './slices/testerSlice';

export const store = configureStore({
  reducer: {
    tester: testerSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
