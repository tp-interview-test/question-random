import {createSlice} from '@reduxjs/toolkit';
import type {PayloadAction} from '@reduxjs/toolkit';

interface List {
  name: string;
  value: number;
}

interface State {
  items: List[];
}

const initialState: State = {
  items: [],
};

export const pointSlice = createSlice({
  name: 'point',
  initialState,
  reducers: {
    add: (state, action: PayloadAction<List>) => {
      state.items = [...state.items, action.payload];
    },
  },
});

export const {add} = pointSlice.actions;

export default pointSlice.reducer;
