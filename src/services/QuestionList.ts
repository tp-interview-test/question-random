export type Choise = {
  id: number;
  title: string;
  question?: Qustion;
};

export type Qustion = {
  id: number;
  title: string;
  answer: number;
  choices: Choise[];
  choiceSelectId?: number;
};

export const qustionList: Qustion[] = [
  {
    id: 1,
    title: '100 + 200 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '300',
      },
      {
        id: 2,
        title: '200',
      },
      {
        id: 3,
        title: '400',
      },
      {
        id: 4,
        title: '500',
      },
    ],
  },
  {
    id: 2,
    title: '200 + 200 = ?',
    answer: 2,
    choices: [
      {
        id: 1,
        title: '1,000',
      },
      {
        id: 2,
        title: '400',
      },
      {
        id: 3,
        title: '300',
      },
      {
        id: 4,
        title: '5,000',
      },
    ],
  },
  {
    id: 3,
    title: '1,000 + 200 = ?',
    answer: 3,
    choices: [
      {
        id: 1,
        title: '1,000',
      },
      {
        id: 2,
        title: '1,100',
      },
      {
        id: 3,
        title: '1,200',
      },
      {
        id: 4,
        title: '1,300',
      },
    ],
  },
  {
    id: 4,
    title: '10 + 50 = ?',
    answer: 4,
    choices: [
      {
        id: 1,
        title: '30',
      },
      {
        id: 2,
        title: '40',
      },
      {
        id: 3,
        title: '50',
      },
      {
        id: 4,
        title: '60',
      },
    ],
  },
  {
    id: 5,
    title: '101 + 200 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '301',
      },
      {
        id: 2,
        title: '201',
      },
      {
        id: 3,
        title: '101',
      },
      {
        id: 4,
        title: '10',
      },
    ],
  },
  {
    id: 6,
    title: '100 + 500 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '600',
      },
      {
        id: 2,
        title: '400',
      },
      {
        id: 3,
        title: '5,000',
      },
      {
        id: 4,
        title: '6,000',
      },
    ],
  },
  {
    id: 7,
    title: '99 + 200 = ?',
    answer: 3,
    choices: [
      {
        id: 1,
        title: '289',
      },
      {
        id: 2,
        title: '290',
      },
      {
        id: 3,
        title: '299',
      },
      {
        id: 4,
        title: '399',
      },
    ],
  },
  {
    id: 8,
    title: '500 + 1 = ?',
    answer: 2,
    choices: [
      {
        id: 1,
        title: '500',
      },
      {
        id: 2,
        title: '501',
      },
      {
        id: 3,
        title: '502',
      },
      {
        id: 4,
        title: '503',
      },
    ],
  },
  {
    id: 9,
    title: '100 + 1,000 = ?',
    answer: 4,
    choices: [
      {
        id: 1,
        title: '1,000',
      },
      {
        id: 2,
        title: '1,200',
      },
      {
        id: 3,
        title: '1,300',
      },
      {
        id: 4,
        title: '1,100',
      },
    ],
  },
  {
    id: 10,
    title: '1,000 + 2,000 = ?',
    answer: 2,
    choices: [
      {
        id: 1,
        title: '2,000',
      },
      {
        id: 2,
        title: '3,000',
      },
      {
        id: 3,
        title: '4,000',
      },
      {
        id: 4,
        title: '5,000',
      },
    ],
  },
  {
    id: 11,
    title: '1 + 1 = ?',
    answer: 2,
    choices: [
      {
        id: 1,
        title: '1',
      },
      {
        id: 2,
        title: '2',
      },
      {
        id: 3,
        title: '3',
      },
      {
        id: 4,
        title: '4',
      },
    ],
  },
  {
    id: 12,
    title: '2 + 6 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '8',
      },
      {
        id: 2,
        title: '9',
      },
      {
        id: 3,
        title: '7',
      },
      {
        id: 4,
        title: '6',
      },
    ],
  },
  {
    id: 13,
    title: '4 + 5 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '9',
      },
      {
        id: 2,
        title: '19',
      },
      {
        id: 3,
        title: '1',
      },
      {
        id: 4,
        title: '20',
      },
    ],
  },
  {
    id: 14,
    title: '100 + 555 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '655',
      },
      {
        id: 2,
        title: '666',
      },
      {
        id: 3,
        title: '555',
      },
      {
        id: 4,
        title: '456',
      },
    ],
  },
  {
    id: 15,
    title: '15 + 15 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '30',
      },
      {
        id: 2,
        title: '40',
      },
      {
        id: 3,
        title: '50',
      },
      {
        id: 4,
        title: '60',
      },
    ],
  },
  {
    id: 16,
    title: '55 + 45 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '100',
      },
      {
        id: 2,
        title: '99',
      },
      {
        id: 3,
        title: '101',
      },
      {
        id: 4,
        title: '102',
      },
    ],
  },
  {
    id: 17,
    title: '2 + 3 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '5',
      },
      {
        id: 2,
        title: '6',
      },
      {
        id: 3,
        title: '7',
      },
      {
        id: 4,
        title: '8',
      },
    ],
  },
  {
    id: 18,
    title: '88 + 12 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '100',
      },
      {
        id: 2,
        title: '102',
      },
      {
        id: 3,
        title: '202',
      },
      {
        id: 4,
        title: '98',
      },
    ],
  },
  {
    id: 19,
    title: '99 + 99 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '198',
      },
      {
        id: 2,
        title: '189',
      },
      {
        id: 3,
        title: '199',
      },
      {
        id: 4,
        title: '200',
      },
    ],
  },
  {
    id: 20,
    title: '5 + 5 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '10',
      },
      {
        id: 2,
        title: '15',
      },
      {
        id: 3,
        title: '20',
      },
      {
        id: 4,
        title: '25',
      },
    ],
  },
  {
    id: 21,
    title: '10 + 1 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '11',
      },
      {
        id: 2,
        title: '12',
      },
      {
        id: 3,
        title: '13',
      },
      {
        id: 4,
        title: '14',
      },
    ],
  },
  {
    id: 22,
    title: '22 + 22 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '44',
      },
      {
        id: 2,
        title: '33',
      },
      {
        id: 3,
        title: '22',
      },
      {
        id: 4,
        title: '55',
      },
    ],
  },
  {
    id: 23,
    title: '23 + 32 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '55',
      },
      {
        id: 2,
        title: '66',
      },
      {
        id: 3,
        title: '56',
      },
      {
        id: 4,
        title: '65',
      },
    ],
  },
  {
    id: 24,
    title: '24 + 42 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '66',
      },
      {
        id: 2,
        title: '55',
      },
      {
        id: 3,
        title: '44',
      },
      {
        id: 4,
        title: '77',
      },
    ],
  },
  {
    id: 25,
    title: '25 + 52 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '77',
      },
      {
        id: 2,
        title: '66',
      },
      {
        id: 3,
        title: '55',
      },
      {
        id: 4,
        title: '44',
      },
    ],
  },
  {
    id: 26,
    title: '26 + 62 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '88',
      },
      {
        id: 2,
        title: '99',
      },
      {
        id: 3,
        title: '98',
      },
      {
        id: 4,
        title: '89',
      },
    ],
  },
  {
    id: 27,
    title: '27 + 3 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '30',
      },
      {
        id: 2,
        title: '31',
      },
      {
        id: 3,
        title: '32',
      },
      {
        id: 4,
        title: '33',
      },
    ],
  },
  {
    id: 28,
    title: '28 + 82 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '110',
      },
      {
        id: 2,
        title: '120',
      },
      {
        id: 3,
        title: '100',
      },
      {
        id: 4,
        title: '115',
      },
    ],
  },
  {
    id: 29,
    title: '29 + 92 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '121',
      },
      {
        id: 2,
        title: '122',
      },
      {
        id: 3,
        title: '112',
      },
      {
        id: 4,
        title: '221',
      },
    ],
  },
  {
    id: 30,
    title: '30 + 333 = ?',
    answer: 1,
    choices: [
      {
        id: 1,
        title: '360',
      },
      {
        id: 2,
        title: '306',
      },
      {
        id: 3,
        title: '603',
      },
      {
        id: 4,
        title: '630',
      },
    ],
  },
];
