import React, {useEffect} from 'react';
import {View, Text, Button, FlatList, StyleSheet} from 'react-native';
import type {NativeStackScreenProps} from '@react-navigation/native-stack';
import {useSelector} from 'react-redux';
import {RootState} from '../redux/store';

type Props = NativeStackScreenProps<any, any>;

const Item = ({index, name, value}: any) => (
  <View style={styles.itemContainer}>
    <View style={styles.nameContainer}>
      <Text>{index + 1}. </Text>
      <Text>{name}</Text>
    </View>
    <View style={styles.pointContainer}>
      <Text style={styles.pointText}>{value}</Text>
    </View>
  </View>
);

export default function HomeScreen({navigation}: Props): React.JSX.Element {
  const testerList = useSelector((state: RootState) => state.tester.items);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button title="ทำข้อสอบ" onPress={() => navigation.navigate('Test')} />
      ),
    });
  }, [navigation]);

  const list = [...testerList];
  list.sort((a, b) => b.value - a.value || a.name.localeCompare(b.name));

  return (
    <FlatList
      data={list}
      renderItem={({item, index}) => <Item index={index} {...item} />}
      keyExtractor={(item, index) => index.toString()}
    />
  );
}

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    paddingVertical: 10,
    paddingHorizontal: 10,
    paddingTop: 20,
  },
  nameContainer: {
    flex: 0.7,
    flexDirection: 'row',
  },
  pointContainer: {
    flex: 0.3,
  },
  pointText: {
    textAlign: 'center',
  },
});
