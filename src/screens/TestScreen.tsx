import React, {useEffect, useState, useCallback, useRef} from 'react';
import {
  View,
  Text,
  Button,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  Alert,
  TextInput,
  StyleSheet,
  AppState,
} from 'react-native';
import type {NativeStackScreenProps} from '@react-navigation/native-stack';
import lodash from 'lodash';

import {useDispatch} from 'react-redux';

import {add} from '../redux/slices/testerSlice';
import {Choise, Qustion, qustionList} from '../services/QuestionList';

type Props = NativeStackScreenProps<any, any>;

export function getQuestions() {
  const questions = lodash.sampleSize(qustionList, 20);
  questions.map(item => {
    item.choices = lodash.sampleSize(item.choices, 4);
    return item;
  });
  return questions;
}

const Item = ({index, id, title, choiceSelectId, choices, onSelect}: any) => (
  <View style={styles.itemContainer}>
    <View style={styles.question}>
      <Text>ข้อที่ {index + 1}) </Text>
      <Text>{title}</Text>
    </View>
    <View style={styles.choiceContainer}>
      {choices.map((choise: Choise, cIndex: number) => (
        <TouchableOpacity
          key={cIndex}
          style={[
            styles.choiceItem,
            choiceSelectId === choise.id && styles.choiceItemSelected,
          ]}
          onPress={() =>
            onSelect({
              ...choise,
              question: {
                id: id,
              },
            })
          }>
          <Text
            style={[
              styles.choiceText,
              choiceSelectId === choise.id && styles.choiceTextSelected,
            ]}>
            {choise.title}
          </Text>
        </TouchableOpacity>
      ))}
    </View>
  </View>
);

export default function TestScreen({navigation}: Props): React.JSX.Element {
  const dispatch = useDispatch();

  const inputRef = useRef(null);

  const appState = useRef(AppState.currentState);

  const [testerName, onChangeTesterName] = useState<string>();
  const [questionList, setQuestionList] = useState<Qustion[]>([]);
  const [refreshing, setRefreshing] = useState<boolean>(false);

  const handleSubmit = useCallback(() => {
    if (!testerName) {
      Alert.alert('ระบุชื่อผู้ทำข้อสอบ');
      inputRef && inputRef.current.focus();
      return;
    }

    const count = questionList.filter(
      item => item.answer === item.choiceSelectId,
    ).length;

    Alert.alert(`ตอบถูก ${count} ข้อ`, '', [
      {
        text: 'OK',
        onPress: () => {
          dispatch(add({name: testerName, value: count}));
          navigation.goBack();
        },
      },
    ]);
  }, [navigation, dispatch, questionList, testerName]);

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    setQuestionList(getQuestions());
    setTimeout(() => {
      setRefreshing(false);
    }, 1000);
  }, []);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => <Button title="ส่งคำตอบ" onPress={handleSubmit} />,
    });
  }, [navigation, handleSubmit]);

  useEffect(() => {
    setQuestionList(getQuestions());
  }, []);

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === 'active'
      ) {
        onRefresh();
        console.log('App has come to the foreground!');
      }

      appState.current = nextAppState;
      console.log('AppState', appState.current);
    });

    return () => {
      subscription.remove();
    };
  }, [onRefresh]);

  const handleOnSelect = (choice: Choise) => {
    const list = lodash.cloneDeep(questionList);

    if (choice.question?.id) {
      list.map(item => {
        if (item.id === choice.question?.id) {
          item.choiceSelectId = choice.id;
        }

        return item;
      });
    }

    setQuestionList(list);
  };

  return (
    <View>
      <FlatList
        data={questionList}
        renderItem={({item, index}) => (
          <Item index={index} onSelect={handleOnSelect} {...item} />
        )}
        keyExtractor={item => item.id.toString()}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        ListHeaderComponent={
          <View style={styles.inputContainer}>
            <Text>ชื่อผู้ทำข้อสอบ</Text>
            <TextInput
              ref={inputRef}
              style={styles.input}
              onChangeText={onChangeTesterName}
              value={testerName}
            />
          </View>
        }
      />
    </View>
  );
}

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  input: {
    flex: 1,
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  itemContainer: {
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    paddingVertical: 10,
    paddingHorizontal: 10,
    paddingTop: 20,
  },
  question: {
    flexDirection: 'row',
    alignItems: 'baseline',
  },
  choiceContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 20,
    marginTop: 10,
  },
  choiceItem: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 10,
    marginBottom: 10,
    marginLeft: 10,
    paddingVertical: 10,
    width: '45%',
  },
  choiceItemSelected: {
    backgroundColor: 'green',
  },
  choiceText: {
    textAlign: 'center',
  },
  choiceTextSelected: {
    color: '#fff',
  },
});
